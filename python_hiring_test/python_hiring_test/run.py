"""Main script for generating output.csv."""

# libraries pandas and sqlite3 used
import pandas as pd
import sqlite3 as sq

# function to return Side and Left or right on the hitterSide/pitcherSide
# Input : Split
# Output : string array of 2


def hitter_pitcher(split):
    if(split == "vs RHP"):
        return "R", "PitcherSide"

    elif(split == "vs LHP"):
        return "L", "PitcherSide"

    elif(split == "vs RHH"):
        return "R", "HitterSide"

    else:
        return "L", "HitterSide"

# function to calculate Slugging Percentage for the given combination
# Input : Subject, Split
# Output : Data frame of the computed SLG for the given combination


def slug_percentage(subject, split):

# Fuction call to get Side and Left or right on the hitterside/pitcherside
    split_L_R, side = hitter_pitcher(split)
    
# Sql template string defined with placeholders 
# so that it can be cater to all commbinations computing  SLG 

    sql = '''SELECT ~SubjectId~ AS SubjectId, 'SLG' AS Stat, '~Split~' AS Split,
           '~SubjectId~' AS Subject,
           CAST(SUM(TB) AS FLOAT)/SUM(AB) AS Value
           FROM sport_data
           WHERE ~Side~ = '~Pitcher_Hitter~'
           GROUP BY ~SubjectId~
           HAVING SUM(PA) >= 25'''

    sql = sql.replace("~SubjectId~", subject)

    sql = sql.replace("~Split~", split)

    sql = sql.replace("~Side~", side)

    sql = sql.replace("~Pitcher_Hitter~", split_L_R)

    return pd.read_sql_query(sql, conn)


# function to calculate Average  for the given combination
# Input : Subject, Split
# Output : Data frame of the computed Average for the given combination


def average(subject, split):
    
# Fuction call to get Side and Left or right on the hitterside/pitcherside
    split_L_R, side = hitter_pitcher(split)
    
# Sql template string defined with placeholders 
# so that it can be cater to all commbinations computing  Average 

    sql = '''SELECT ~SubjectId~ AS SubjectId, 'AVG' AS Stat, '~Split~' AS Split,
           '~SubjectId~' AS Subject,
           CAST(SUM(H) AS FLOAT)/SUM(AB) AS Value
           FROM sport_data
           WHERE ~Side~ = '~Pitcher_Hitter~'
           GROUP BY ~SubjectId~
           HAVING SUM(PA) >= 25'''

    sql = sql.replace("~SubjectId~", subject)

    sql = sql.replace("~Split~", split)

    sql = sql.replace("~Side~", side)

    sql = sql.replace("~Pitcher_Hitter~", split_L_R)

    return pd.read_sql_query(sql, conn)


# function to calculate OBP for the given combination
# Input : Subject, Split
# Output : Data frame of the computed OBP for the given combination


def on_base_percentage(subject, split):
    
# Fuction call to get Side and Left or right on the hitterside/pitcherside
    split_L_R, side = hitter_pitcher(split)
    
# Sql template string defined with placeholders 
# so that it can be cater to all commbinations computing OBP 

    sql = '''SELECT ~SubjectId~ AS SubjectId, 'OBP' AS Stat, '~Split~' AS Split,
           '~SubjectId~' AS Subject,
           ROUND(((CAST(SUM(H)+SUM(BB)+SUM(HBP) AS FLOAT))/(CAST(SUM(AB)+SUM(BB)+SUM(HBP)+SUM(SF) AS FLOAT))),5) AS Value
           FROM sport_data
           WHERE ~Side~ = '~Pitcher_Hitter~'
           GROUP BY ~SubjectId~
           HAVING SUM(PA) >= 25'''

    sql = sql.replace("~SubjectId~", subject)

    sql = sql.replace("~Split~", split)

    sql = sql.replace("~Side~", side)

    sql = sql.replace("~Pitcher_Hitter~", split_L_R)

    return pd.read_sql_query(sql, conn)


# function to calculate OPS for the given combination
# Input : Subject, Split
# Output : Data frame of the computed OBS for the given combination
def on_base_slugging(subject, split):
# Fuction call to get Side and Left or right on the hitterside/pitcherside
    split_L_R, side = hitter_pitcher(split)
    
# Sql template string defined with placeholders 
# so that it can be cater to all commbinations computing OBS 

    sql = '''SELECT ~SubjectId~ AS SubjectId, 'OPS' AS Stat, '~Split~' AS Split,
           '~SubjectId~' AS Subject,
           ROUND(CAST(SUM(AB) * (SUM(H)+SUM(BB)+SUM(HBP))+SUM(TB)*(SUM(AB)+SUM(BB)+SUM(SF)+SUM(HBP)) AS FLOAT)/(SUM(AB)*(SUM(AB)+SUM(BB)              +SUM(SF)+SUM(HBP))),3) AS Value
           FROM sport_data
           WHERE ~Side~ = '~Pitcher_Hitter~'
           GROUP BY ~SubjectId~
           HAVING SUM(PA) >= 25'''

    sql = sql.replace("~SubjectId~", subject)

    sql = sql.replace("~Split~", split)

    sql = sql.replace("~Side~", side)

    sql = sql.replace("~Pitcher_Hitter~", split_L_R)

    return pd.read_sql_query(sql, conn)

# main function 


def main():

# the pitchdata csv file is read and stored into sqlite database
    data = pd.read_csv("./data/raw/pitchdata.csv")
    global conn, c1
    conn = sq.connect("python_test.db")
    data.to_sql(name="sport_data", con=conn, if_exists="replace", index=False)
    c1 = conn.cursor()
# Combinations txt read and iterated.
    file = open("./data/reference/combinations.txt")
    lines = file.readlines()[1:]
    avg = pd.DataFrame()
    obp = pd.DataFrame()
    ops = pd.DataFrame()
    slg = pd.DataFrame()
    for val in lines:
        a = val.strip("\n").split(",")
# Appropriate function called for each iteration based computation mentioned
        if (a[0] == 'AVG'):
            result_avg = average(a[1], a[2])
            avg = avg.append(result_avg)

        elif (a[0] == 'OBP'):
            result_obp = on_base_percentage(a[1], a[2])
            obp = obp.append(result_obp)

        elif (a[0] == 'SLG'):
            result_slg = slug_percentage(a[1], a[2])
            slg = slg.append(result_slg)

        else:
            result_ops = on_base_slugging(a[1], a[2])
            ops = ops.append(result_ops)
    frames = [avg, obp, slg, ops]
# Data frames  concantenated for all computations across all combinations
    
    output = pd.concat(frames)
    
# Output rows sorted across multiple columns as per specifcation
    output = output.sort_values(['SubjectId', 'Stat', 'Split', 'Subject', 'Value'], ascending=[True, True, True, True, False])
#decimal values rounded to 3 decimal places
# pandas round function called instead of SQLite round function
#as SQLite produces floor values for 0.5555 values
    final_output=output.round(3)
#Final output  written to output csv file using relative path
    final_output.to_csv("./data/processed/output.csv", index=False)
    c1.close()

if __name__ == '__main__':
    main()
